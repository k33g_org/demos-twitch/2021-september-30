# 2021 September 30

## Grain

- https://grain-lang.org/
- https://www.infoq.com/news/2021/05/grain-web-assembly-first/
- https://twitter.com/Philip_E_Blair
- https://twitter.com/oscar_spen
- https://twitter.com/blainebublitz
- https://github.com/grain-lang/grain/issues/936

## Wasmer

- https://wasmer.io/

## Wagi (Deislab)

- https://github.com/deislabs/wagi

Grain, Wasm, Wasmer, Wagi

- Présentation de Grain
- Compilation et exécution avec Wasmer
- Compilation (autre exemple) et utilisation de Wagi

