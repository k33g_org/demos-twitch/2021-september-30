- https://github.com/deislabs/wagi
- https://github.com/deislabs/wagi-examples

## Install wagi

```bash
wget https://github.com/deislabs/wagi/releases/download/v0.4.0/wagi-v0.4.0-linux-amd64.tar.gz
tar -zxf wagi-v0.4.0-linux-amd64.tar.gz
rm wagi-v0.4.0-linux-amd64.tar.gz
```

## Run and test

```bash
./wagi -c ./modules.toml
curl $(gp url 3000)/hello-grain
curl $(gp url 3000)/hello-grain?message=hello
```

